module.exports = {
  siteMetadata: {
    title: 'Zamobomi',
    siteUrl:"https://www.zamobomi.co.za"
  },
  plugins: [
    'gatsby-plugin-sitemap',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-sass',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: 'Zamobomi Trading',
        short_name: 'Zamobomi',
        start_url: '/',
        background_color: '#3254ca',
        theme_color: '#3254ca',
        display: 'minimal-ui',
        icon: 'src/images/logo.png', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-offline',
  ],
}
