import React from 'react'
import { Link } from 'gatsby'
import worker from  '../images/MaleWorkerHoldingMop.jpg'
import Layout from '../components/layout'
import construction from '../images/Realpeopleworking.jpg';
import cleaning from '../images/office.jpg';
import carpentry from '../images/blue.jpg';
import paving from '../images/worker.jpg';
import officecleaning from '../images/cleaner.jpg';
import './style.scss'
const IndexPage = () => (
  <Layout>
    <div className='main'>
     <div className='tagliner'> 
       <h1><strong> Cleanliness is our passion </strong> </h1>
       <div className='cta'>
        <Link to='quote' ><button className='btn'>Get Quote </button></Link>
        <Link to='consultant'> <button className='btn'>Give you a call?</button></Link>
       </div>
       <br />
       <p> Helping you clean </p>
     </div>
      <img src={worker} />
    </div>
    <div className='services'>
      <h1> Our offer to you</h1>
    </div>
    <div className="offers">
      <div className='construction'>
        <img src={construction} />
        <div className='text'>
          <h2>Construction</h2>
          <p> 
            Zamobomi offers top of the market construction services  at affordable prices
            We are fully committed to providing our clients with services unmatched in the industry. 
          </p>
          <Link to='quote' ><button className='btn'>Get Quote </button></Link>
        </div>
      </div>
      <div className='cleaning'>
        <img src={cleaning} />
        <div className='text'>
          <h2>Cleaning</h2>
          <p> Zamobomi also offers affordable cleaning services</p>
          <p> 
            We understand the hastles of hosting events and the day to day runnings of corporate entitties and can 
            help ease your concenrs with our affordable cleaning services
            Known for our professionalism, reliability, fairness, and meticulous attention to detail,
            we are consistently recommended by our customers, and receive many referrals and repeat business</p>
            <Link to='quote' ><button className='btn'>Get Quote </button></Link>
        </div>
      </div>
      <div className='construction'>
        <img src={paving} />
        <div className='text'>
          <h2>Paving</h2>
          <p> 
            From small businesses to large shopping centers, and from schools to apartment complexes, our team at Zamobomi Trading
            has commercial paving options for you. Our paving projects are planned and executed in stages, in order for you to be able
            to stay open for business while we work. We work closely with you to develop a layout and plan for completing your paving 
            project on your schedule. From the planning stage to the final walkthrough and evaluation, our team will work with you 
            throughout the entire process. Call us today for a free estimate on your paving project to get the job done right and within your
            budget.
          </p>
          <Link to='quote' ><button className='btn'>Get Quote </button></Link>
        </div>
      </div>
      <div className='cleaning'>
        <img src={carpentry} />
        <div className='text'>
          <h2>Carpentry</h2>
          <p> We have the experience to complete even the most difficult projects with courteous professional service yielding the highest quality finished product. </p>
          <Link to='quote' ><button className='btn'>Get Quote </button></Link>
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage
