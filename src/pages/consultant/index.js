import React from 'react'
import Layout from '../../components/layout'
import './style.scss'
import femalearch from '../../images/femalearch.jpg';
import { hotjar } from 'react-hotjar';
import config from '../../config';
class CallPage extends React.Component{
    constructor(){
        super();
        this.onClick = this.onClick.bind(this);
        this.state = { done: false }
    }
    onClick(e){
        const scriptURL = 'https://script.google.com/macros/s/AKfycbxO5lCtExnB9ngxjti2hCK919I925uQ9JhtfyYWLnzKI_Wa5FnF/exec';
        const form = document.forms['submit-to-google-sheet']
        fetch(scriptURL, { method: 'POST', body: new FormData(form)})
            .then(response => console.log('Success!', response))
            .catch(error => console.error('Error!', error.message))
        this.setState({ done: true })
    }

    componentDidMount(){
        if (typeof window !== `undefined`) {
            const ReactGA = require('react-ga');
            const hotjar = require('react-hotjar').hotjar;
            ReactGA.initialize('UA-128511035-1')
            ReactGA.pageview(window.location.pathname + window.location.search)
            hotjar.initialize(1073781, 6);
            ReactGA.event({
                category: 'User',
                action: 'Meet Consultant'
            })
        }
    }
    render(){
        const { provinces } = config;
        return (
            <Layout>
                <div className='consult-main'>
                    <div className='consult-image'>
                        <img src={femalearch } />
                    </div>
                    <div className='consult-info'>
                        <h1> Speak to one of our friendly consultants </h1>
                        <br />
                        <p>Our knowledgeable staff is prepared to answer any questions you may have. We look forward to hearing from you and working with you in the future. </p>
                        <br />
                        <div className='quote-form'>
                            <form name='submit-to-google-sheet'>
                                    <div className='input-wrapper'>
                                        <label for='fullname'>Fullname</label>
                                        <input type='text' name='fullname'/>
                                    </div>
                                    <div className='input-wrapper'>
                                        <label for='province'>Province</label>
                                        <select name='province'>
                                            {
                                                provinces.map(province => <option value={province.id} key={province.id}>{province.name}</option>)
                                            }
                                        </select>
                                    </div>
                                    <div className='input-wrapper'>
                                        <label for='cellphon_number'>Cellphone Number</label>
                                        <input type='text-' name='cellphone_number'/>
                                    </div>  
                                    <button type="button" className='btn' onClick={this.onClick}>Done </button>
                            </form>
                            {this.state.done &&
                                <div className='done'>
                                        <h1>Awesome</h1>
                                        <p> We'll be in touch </p>
                                </div>
                            }
                        </div>
                    </div>
                </div>
            </Layout>
        )
    }
}

export default CallPage
