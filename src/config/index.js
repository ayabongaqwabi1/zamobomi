const provinces =[
    {
        id: 'ec',
        name:'Eastern Cape'
    },
    {
        id: 'gp',
        name:'Gauteng'
    },
    {
        id: 'wc',
        name:'Western Cape'
    },
    {
        id: 'np',
        name:'Northern Cape'
    },
    {
        id: 'fs',
        name:'Free State'
    },
    {
        id: 'kzn',
        name:'Kwa-Zulu Natal'
    },
    {
        id: 'nw',
        name:'North West'
    },
    {
        id: 'lp',
        name:'Limpopo'
    },
    {
        id: 'mp',
        name:'Mpumalanga'
    }
]
const services = [
    {
        id:'zam-clean',
        name:' Cleaning'
    },
    {
        id:'zam-carpentry',
        name:' Carpentry'
    },
    {
        id:'zam-pave',
        name:'Paving'
    },
    {
        id:'zam-construct',
        name:' Construction'
    }
]

export default {
    services,
    provinces,
}