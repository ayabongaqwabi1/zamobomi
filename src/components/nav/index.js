import React, {Component} from 'react'
import './style.scss';
import Link from 'gatsby-link';
export default class Nav extends Component {
    constructor(props){
        super(props);
        this.toggleOpen = this.toggleOpen.bind(this)
        this.state={ open: false }
    }

    toggleOpen(){
        console.log('togggling!')
        this.setState({open: !this.state.open});
    }
    render(){
        const collapseClass = this.state.open ? 'open-nav' : 'closed-nav';
        return(
            <nav className="navbar navbar-expand-lg fixed-top navbar-transparent" color-on-scroll="300">
                <div className="container">
                    <div className="navbar-translate" >
                        <Link to='/' className="navbar-brand">Zamobomi</Link>
                        <button className="navbar-toggler navbar-toggler-right navbar-burger" onClick={this.toggleOpen}>
                            <span className="navbar-toggler-bar"></span>
                            <span className="navbar-toggler-bar"></span>
                            <span className="navbar-toggler-bar"></span>
                        </button>
                    </div>
                    <div className={`${collapseClass} collapse navbar-collapse`} onClick={this.toggleOpen}>
                        <ul className="navbar-nav ml-auto">
                            {/* <Link to='/'>
                                <li className="nav-item">
                                    <a href="#" className="nav-link"><i className="nc-icon nc-settings"></i>Services</a>
                                </li>
                            </Link>
                           <Link to='/'>
                                <li className="nav-item">
                                    <a href="#" className="nav-link"><i className="nc-icon nc-shop"></i>About</a>
                                </li>
                            </Link>
                            <Link to='/'>
                                <li className="nav-item">
                                    <a href="#" target="_blank" className="nav-link"><i className="nc-icon nc-book-bookmark"></i>  Contact</a>
                                </li>
                            </Link>
                            <li className="nav-item">
                                <a className="nav-link" rel="tooltip" title="Follow us on Twitter" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank">
                                    <i className="fa fa-twitter"></i>
                                    <p className="d-lg-none">Twitter</p>
                                </a>
                            </li> */}
                            <li className="nav-item">
                                <a className="nav-link" rel="tooltip" title="Like us on Facebook" data-placement="bottom" href="https://www.facebook.com/zamobomi" target="_blank">
                                    <i className="fa fa-facebook-square"></i>
                                    <p className="d-lg-none">Facebook</p>
                                </a>
                            </li>
                            {/* <li className="nav-item">
                                <a className="nav-link" rel="tooltip" title="Follow us on Instagram" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank">
                                    <i className="fa fa-instagram"></i>
                                    <p className="d-lg-none">Instagram</p>
                                </a>
                            </li> */}
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }
    
}