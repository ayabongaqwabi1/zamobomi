import React,{Component} from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import metaImage  from '../images/quality.png'
import Header from './header/header'
import Nav from './nav'

import "../assets/css/bootstrap.min.css"
import "../assets/css/paper-kit.css?v=2.1.0" 
import "../assets/css/nucleo-icons.css"
import { hotjar } from 'react-hotjar';



class Layout extends Component {
      componentDidMount(){
        if (typeof window !== `undefined`) {
          const ReactGA = require('react-ga');
          ReactGA.initialize('UA-128511035-1')
          ReactGA.pageview(window.location.pathname + window.location.search)
          hotjar.initialize(1073781, 6);
        }
        
      }
      render(){
        return(
          <div>
            <Helmet
              title={"Zamobomi"}
              meta={[
                { name: 'description', content: ' We have the experience to complete even the most difficult projects with courteous professional service yielding the highest quality finished product' },
                { name: 'keywords', content: 'construction,  paving, carpentry, commercial cleaning' },
                { name: 'og:image', content: metaImage },
              ]}
              script={[
                {"src": "https://wzrd.in/standalone/formdata-polyfill", "type": "text/javascript"}
              ]}
              script={[
                {"src": "https://wzrd.in/standalone/promise-polyfill@latest", "type": "text/javascript"}
              ]}
              script={[
                {"src": "https://wzrd.in/standalone/whatwg-fetch@latest", "type": "text/javascript"}
              ]}
            >
              <html lang="en" />
              <link href='https://fonts.googleapis.com/css?family=Montserrat:400,300,700' rel='stylesheet' type='text/css' />
              <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet" />
              <meta name="google-site-verification" content="c_vopkaZKBV00C6zDFQonGXA-5tgfSJDOKsKTGtDmEk" />
              <meta name='viewport' content="width=device-width, initial-scale=1"/>
            </Helmet>
            <Nav />
            <div>
              {this.props.children}
            </div>
          </div>
        )
      }
}


export default Layout
